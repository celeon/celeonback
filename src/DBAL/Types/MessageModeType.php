<?php


namespace App\DBAL\Types;


use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class MessageModeType extends AbstractEnumType
{
    public const REPEAT = 'repeat';
    public const ONCE = 'once';

    protected static $choices = [
        self::REPEAT => 'repeat',
        self::ONCE => 'once'
    ];
}
