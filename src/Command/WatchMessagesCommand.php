<?php

namespace App\Command;

use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class WatchMessagesCommand extends Command
{
    protected static $defaultName = 'app:watch:messages';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var PublisherInterface
     */
    private $publisher;

    public function __construct(EntityManagerInterface $em, PublisherInterface $publisher)
    {
        parent::__construct();
        $this->em = $em;
        $this->publisher = $publisher;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $today = new \DateTimeImmutable();
        $messages = $this->em->getRepository(Message::class)->getAllToSend($today);
        $messagesToSend = [];
        foreach ($messages as $message) {
            $encMessage = [
              'id' => $message->getId(),
              'content' => $message->getContent(),
              'channel' => $message->getChannel(),
              'providerId' => $message->getServer()->getProviderId(),
            ];

            $messagesToSend[] = $encMessage;
        }
        if(!empty($messagesToSend)) {
            $update = new Update(
                'http://example.com/messages',
                json_encode($messagesToSend)
            );

            $publisher = $this->publisher;
            $publisher($update);
        }

        return 0;
    }
}
