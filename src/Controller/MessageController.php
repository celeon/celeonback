<?php

namespace App\Controller;

use App\DBAL\Types\MessageModeType;
use App\Entity\Message;
use App\Entity\Server;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/{providerId}/message")
 */
class MessageController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="message_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator, $providerId): Response
    {
        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $providerId]);
        $query = $this->em->getRepository(Message::class)->findAllQuery();

        $paginatedMessages = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('message/index.html.twig', [
            'server' => $server,
            'paginatedMessages' => $paginatedMessages,
        ]);
    }

    /**
     * @Route("/new", name="message_new", methods={"GET","POST"})
     */
    public function new(Request $request, $providerId): Response
    {
        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $providerId]);

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setServer($server);
            $message->setMode(MessageModeType::ONCE);
            $message->setIsSent(false);
            $this->em->persist($message);
            $this->em->flush();

            return $this->redirectToRoute('message_index', ['providerId' => $server->getProviderId()]);
        }

        return $this->render('message/new.html.twig', [
            'server' => $server,
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="message_show", methods={"GET"})
     */
    public function show(Message $message): Response
    {
        return $this->render('message/show.html.twig', [
            'message' => $message,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="message_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Message $message, $providerId): Response
    {
        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $providerId]);
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('message_index', ['providerId' => $server->getProviderId()]);
        }

        return $this->render('message/edit.html.twig', [
            'server' => $server,
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="message_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Message $message, $providerId): Response
    {
        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $providerId]);
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($message);
            $entityManager->flush();
        }

        return $this->redirectToRoute('message_index', ['providerId' => $server]);
    }
}
