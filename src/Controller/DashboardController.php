<?php

namespace App\Controller;

use App\Entity\Server;
use App\Model\Discord\RolePermission;
use App\Services\Discord\ApiService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 */
class DashboardController extends AbstractController
{
    /**
     * @var ApiService
     */
    private $discordService;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, ApiService $discordService)
    {
        $this->discordService = $discordService;
        $this->em = $em;
    }

    /**
     * @Route("/dashboard/servers", name="discord_servers")
     */
    public function discordServers()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $displayServers = [];
        $this->get('session')->remove('userServers');
        $servers = $this->discordService->getUserServers();
        $this->get('session')->set('userServers', $servers);

        foreach ($servers as $server) {
            if($server->owner || $server->permissions->hasMultiplePermissions([RolePermission::ADMINISTRATOR, RolePermission::MANAGE_SERVER, RolePermission::MANAGE_MESSAGES, RolePermission::MANAGE_ROLES], RolePermission::OR)) {
                $displayServers[] = $server;
            }
        }
        return $this->render('dashboard/servers.html.twig', [
            'servers' => $displayServers
        ]);
    }

    /**
     * @Route("/dashboard/{serverId}", name="dashboard")
     */
    public function index($serverId)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $serverId]);

        if(!$server) {
            $servers = $this->get('session')->get('userServers');

            $currentServer = array_values(array_filter($servers, static function ($element) use ($serverId) {
               return $element->id === $serverId;
            }));

            if($currentServer[0]->owner) {
                return $this->redirect($this->discordService->getBotInviteUrl($serverId));
            }
            $this->addFlash('danger', "You don't have the permissions to add Celeon to the server <strong>\"$currentServer[0]\"</strong>. Please ask the server owner.");
            return $this->redirectToRoute('discord_servers');

        }

        return $this->redirectToRoute('message_index', ['providerId' => $server->getProviderId()]);
    }

    /**
     * @Route("/authorize/server", name="authorize_discord_server")
     */
    public function authorizeServer(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();
        $serverId = $request->get('guild_id');
        $code = $request->get('code');

        $server = $this->em->getRepository(Server::class)->findOneBy(['providerId' => $serverId]);

        if(!$server) {
            $botToken = $this->discordService->getBotAccessToken($code);
            $server = new Server();
            $server->setProviderId($serverId);
            $server->setName($botToken->getValues()['guild']['name']);
            $server->setOwner($user);
            $server->addMember($user);
            $this->em->persist($server);
            $this->em->flush();
        }

        return $this->redirectToRoute('dashboard', ['serverId' => $server->getProviderId()]);
    }
}
