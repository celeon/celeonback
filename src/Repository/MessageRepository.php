<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function findAllQuery()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.executionDate', 'DESC')
            ->getQuery()
        ;
    }

    public function getAllToSend(\DateTimeImmutable $date) {
        return $this->createQueryBuilder('m')
            ->andWhere('m.isSent = :isSent')
            ->andWhere('m.executionDate <= :date')
            ->orderBy('m.executionDate', 'ASC')
            ->setParameters([
                'isSent' => false,
                'date' => $date->format('Y-m-d H:i:s')
            ])
            ->getQuery()
            ->getResult();
    }
}
