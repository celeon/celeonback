<?php


namespace App\Model\Discord;


class UserGuild
{
    public $id;
    public $name;
    public $icon;
    public $owner;
    /**
     * @var RolePermission
     */
    public $permissions;
    public $features;

    public function __toString()
    {
       return $this->name;
    }
}
