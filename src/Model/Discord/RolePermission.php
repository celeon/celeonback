<?php


namespace App\Model\Discord;


class RolePermission
{
    public const CREATE_INSTANT_INVITE = 'create_instant_invite';
    public const KICK_MEMBERS = 'kick_members';
    public const BAN_MEMBERS = 'ban_members';
    public const ADMINISTRATOR = 'administrator';
    public const MANAGE_CHANNELS = 'manage_channels';
    public const MANAGE_SERVER = 'manage_server';
    public const CHANGE_NICKNAME = 'change_nickname';
    public const MANAGE_NICKNAMES = 'manage_nicknames';
    public const MANAGE_ROLES = 'manage_roles';
    public const READ_MESSAGES = 'read_messages';
    public const SEND_MESSAGES = 'send_messages';
    public const SEND_TTS_MESSAGES = 'send_tts_messages';
    public const MANAGE_MESSAGES = 'manage_messages';
    public const EMBED_LINKS = 'embed_links';
    public const ATTACH_FILES = 'attach_files';
    public const READ_MESSAGE_HISTORY = 'read_message_history';
    public const MENTION_EVERYONE = 'mention_everyone';
    public const VOICE_CONNECT = 'voice_connect';
    public const VOICE_SPEAK = 'voice_speak';
    public const VOICE_MUTE_MEMBERS = 'voice_mute_members';
    public const VOICE_DEAFEN_MEMBERS = 'voice_deafen_members';
    public const VOICE_MOVE_MEMBERS = 'voice_move_members';
    public const VOICE_USE_VAD = 'voice_use_vad';

    public const AND = 'AND';
    public const OR = 'OR';

    private $bitwise = [
        self::CREATE_INSTANT_INVITE => 0,
        self::KICK_MEMBERS          => 1,
        self::BAN_MEMBERS           => 2,
        self::ADMINISTRATOR         => 3,
        self::MANAGE_CHANNELS       => 4,
        self::MANAGE_SERVER         => 5,
        self::CHANGE_NICKNAME       => 26,
        self::MANAGE_NICKNAMES      => 27,
        self::MANAGE_ROLES          => 28,

        self::READ_MESSAGES        => 10,
        self::SEND_MESSAGES        => 11,
        self::SEND_TTS_MESSAGES    => 12,
        self::MANAGE_MESSAGES      => 13,
        self::EMBED_LINKS          => 14,
        self::ATTACH_FILES         => 15,
        self::READ_MESSAGE_HISTORY => 17,
        self::MENTION_EVERYONE     => 18,

        self::VOICE_CONNECT        => 20,
        self::VOICE_SPEAK          => 21,
        self::VOICE_MUTE_MEMBERS   => 22,
        self::VOICE_DEAFEN_MEMBERS => 23,
        self::VOICE_MOVE_MEMBERS   => 24,
        self::VOICE_USE_VAD        => 25,
    ];

    /**
     * @var array
     */
    private $permissions;

    /**
     * @var string
     */
    private $value;

    public function __construct(string $bitwise)
    {
        $this->value = $bitwise;
        $this->permissions = $this->decodeBitwise($bitwise);
    }

    public function decodeBitwise($bitwise): array
    {
        $result = [];

        foreach ($this->bitwise as $key => $value) {
            $result[$key] = ((($bitwise >> $value) & 1) == 1);
        }

        return $result;
    }

    /**
     * @param string $permission
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        if(array_key_exists($permission, $this->permissions)) {
            return $this->permissions[$permission];
        }

        throw new \InvalidArgumentException("Permission for key \"$permission\" doesn't exists.");
    }

    /**
     * @param array $permissions
     * @param string $operator
     * @return bool
     */
    public function hasMultiplePermissions(array $permissions, $operator = self::AND): bool
    {
        $result = false;
        foreach ($permissions as $permission) {
            if($operator === self::AND) {
                $result = $result && $this->hasPermission($permission);
            } elseif ($operator === self::OR) {
                $result = $result || $this->hasPermission($permission);
            } else {
                throw new \InvalidArgumentException("Operator \"$operator\" is invalid.");
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     */
    public function setPermissions(array $permissions): void
    {
        $this->permissions = $permissions;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
