<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\DBAL\Types\MessageModeType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"message_read"}},
 *     collectionOperations={"get"},
 *     itemOperations={"get", "patch"}
 * )
 * @ApiFilter(BooleanFilter::class, properties={"isSent" = "false"})
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"message_read", "server_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"message_read", "server_read"})
     */
    private $content;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     * @Groups({"message_read", "server_read"})
     */
    private $executionDate;

    /**
     * @ORM\Column(type="MessageModeType", length=255)
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\MessageModeType")
     * @Groups({"message_read", "server_read"})
     */
    private $mode;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"message_read", "server_read"})
     */
    private $isSent = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("message_read")
     */
    private $server;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("message_read")
     */
    private $channel;

    public function __construct()
    {
        $this->executionDate = new \DateTimeImmutable();
    }

    public function __toString()
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getExecutionDate(): ?\DateTimeImmutable
    {
        return $this->executionDate;
    }

    public function setExecutionDate(\DateTimeImmutable $executionDate): self
    {
        $this->executionDate = $executionDate;

        return $this;
    }

    public function getMode(): ?string
    {
        return $this->mode;
    }

    public function setMode(string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsSent(bool $isSent): self
    {
        $this->isSent = $isSent;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(?Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function setChannel(string $channel): self
    {
        $this->channel = $channel;

        return $this;
    }
}
