<?php


namespace App\Services\Discord;


use App\Model\Discord\RolePermission;
use App\Model\Discord\UserGuild;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class ApiService
{
    public const SESSION_NAME = 'discord-token';
    public const API_DOMAIN = 'https://discordapp.com/api/v6';
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var ClientRegistry
     */
    private $clientRegistry;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(SessionInterface $session, ClientRegistry $clientRegistry, RouterInterface $router)
    {
        $this->session = $session;
        $this->clientRegistry = $clientRegistry;
        $this->router = $router;
    }

    /**
     * @return AccessToken | null
     */
    public function getToken(): ?AccessToken
    {
        return $this->session->get(self::SESSION_NAME, null);
    }

    public function setToken(AccessToken $token)
    {
        $this->session->set(self::SESSION_NAME, $token);
        return $token;
    }

    public function getBotInviteUrl($guildId = null): string
    {
        $botId = '657219616564576337';
        $permissions = '166912';
        $redirectUri = $this->router->generate('authorize_discord_server', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $baseUrl = "https://discordapp.com/oauth2/authorize?client_id=$botId&scope=bot&permissions=$permissions&response_type=code&redirect_uri=$redirectUri";
        if($guildId !== null) {
            $baseUrl .= "&guild_id=$guildId";
        }

        return $baseUrl;
    }

    public function getBotAccessToken($code)
    {
        $client = $this->clientRegistry->getClient('discord_main');
        $client->setAsStateless();
        return $client->getOAuth2Provider()->getAccessToken('authorization_code', [
            'code' => $code,
            'redirect_uri' => $this->router->generate('authorize_discord_server', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
    }

    public function refreshToken(): ?AccessToken
    {
        $token = $this->getToken();
        if($token) {
            if($token->hasExpired()) {
                $newToken = $this->clientRegistry->getClient('discord_main')->getOAuth2Provider()->getAccessToken('refresh_token', [
                    'refresh_token' => $token->getRefreshToken()
                ]);
                return $this->setToken($newToken);
            }

            return $token;
        }
        throw new AuthenticationCredentialsNotFoundException('Access token not found in session.');
    }

    public function createAuthenticatedHttpClient()
    {
        $token = $this->getToken();
        if($token) {
            if(!$token->hasExpired()) {
                 return HttpClient::createForBaseUri(self::API_DOMAIN ,[
                    'auth_bearer' => $token->getToken()
                ]);
            }
            $this->refreshToken();
            $this->createAuthenticatedHttpClient();
        } else {
            throw new AuthenticationCredentialsNotFoundException('Access token not found in session.');
        }
        return null;
    }

    /**
     * @return UserGuild[]
     */
    public function getUserServers(): array
    {
        $result = [];
        $response = $this->createAuthenticatedHttpClient()->request('GET', self::API_DOMAIN . '/users/@me/guilds');
        $servers = json_decode($response->getContent(), true);

        foreach ($servers as $server) {
            $userGuild = new UserGuild();
            $userGuild->id = $server['id'];
            $userGuild->name = $server['name'];
            $userGuild->icon = $server['icon'];
            $userGuild->owner = $server['owner'];
            $userGuild->permissions = new RolePermission($server['permissions']);
            $userGuild->features = $server['features'];
            $result[] = $userGuild;
        }

        return $result;
    }

}
