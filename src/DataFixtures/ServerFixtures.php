<?php

namespace App\DataFixtures;

use App\Entity\Server;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ServerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('server.owner@test.fr');
        $admin->setDiscordId('141685518265090048');
        $manager->persist($admin);

        $server = new Server();
        $server->setName('AmstradDev');
        $server->setProviderId('656949512409186312');
        $server->setOwner($admin);
        $manager->persist($server);
        $manager->flush();
    }
}
