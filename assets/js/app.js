import $ from 'jquery';
import 'bootstrap';
import flatpickr from "flatpickr";

import '../css/app.scss';
import 'flatpickr/dist/flatpickr.min.css'

flatpickr('.js-datepicker', {
  enableTime: true,
  dateFormat: "Y-m-d H:i",
  time_24hr: true
});

const bannerLogo = document.querySelector('.banner-logo');
if(bannerLogo) {
  const setDate = () => {
    const now = new Date();
    const seconds = now.getSeconds();
    const secondsDegrees = ((seconds / 60) * 360) + 90;
    bannerLogo.style.transform = `rotate(${secondsDegrees}deg)`;
  };

  setInterval(setDate, 1000);
  setDate();
}
