# CeleonBack

## How to setup project for development

1. Copy `.env.dist` file in `.env` at project root path
2. Run `docker-compose up -d` command
3. Install dependencies with `docker-compose exec app composer install` and `npm install`
4. Install assets with `docker-compose exec app php bin/console assets:install`
5. Create database with `docker-compose exec app php bin/console doctrine:database:create`
6. Create database scheme with `docker-compose exec app php bin/console doctrine:migrations:migrate`
7. (Optionnal) You can import test datas with `docker-compose exec app php bin/console doctrine:fixtures:load`
8. Build front assets with `npm run dev`
9. Visit `localhost:86`
