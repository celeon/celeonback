FROM php:7.4-fpm

RUN apt-get -qq update && apt-get install -y -qq curl unzip gnupg2 nginx libpq-dev libcurl4-gnutls-dev libzip-dev libicu-dev g++ libonig-dev libxml2-dev

RUN docker-php-ext-install pdo pdo_pgsql curl zip intl mbstring xml opcache

RUN pecl install apcu && docker-php-ext-enable apcu

RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --filename=composer --install-dir=/usr/local/bin --no-ansi \
    && rm -f /tmp/composer-setup.*

ADD . /var/www/celeon-back
ADD docker/nginx/nginx.conf /etc/nginx/sites-enabled/default
COPY docker/entrypoint.sh /etc/entrypoint.sh
RUN chmod +x /etc/entrypoint.sh

WORKDIR /var/www/celeon-back
EXPOSE 80
ENTRYPOINT ["/etc/entrypoint.sh"]
